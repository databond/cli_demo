import * as session from './interfaces/session_interface'
import ISession from './models/session';
import prompts from 'prompts';

let sessions: ISession[] = [];
async function main() {

    let result = await prompts({
        type: 'select',
        name: 'sessionPrompt',
        message: 'Databond interactive CLI demo.'
    })
}

(async () => {
    console.log('Databond interactive CLI demo.\n');
    await main();
})()