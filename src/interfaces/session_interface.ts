import { Client, TcpClient } from 'msgpack-rpc-node';

export class SessionFunctions {
    client: Client<TcpClient>;
    
    /**
     *
     */
    constructor(client: Client<TcpClient>) {
        this.client = client;
    }
    async request(): Promise<void> {
        await this.client.connect();
        await this.client.call('request')
    }
    
    async newSession(sessionName: string): Promise<number> {
        let id = await this.client.call('session.newWithName', sessionName);
        return id as number;
    }
    
    async closeSession(sessionId: number) {
        await this.client.call('session.close', sessionId);
    }
}
const PORT = 8080;
// const client = new Client(TcpClient, PORT);




