import { Client, RPCError, TcpClient } from 'msgpack-rpc-node';
export class ConnectionFunctions {
    client: Client<TcpClient>
    constructor(client: Client<TcpClient>) {
        this.client = client;
    }
    async createConnection(sessionId: number, conName: string, pluginName = 'BondSQLITE') {
        await this.client.call('connection.addConnection', sessionId, conName, pluginName);
    }

    async openConnection(sessionId: number, conName: string): Promise<boolean | RPCError> {
        return false
    }

    async executeQuery(sessionId: number, conName: string, query: string): Promise<string> {
        return ''
    }

    async closeConnection(sessionId: number, conName: string) {

    }
    async removeConnection(sessionId: number, conName: string) {

    }

}



