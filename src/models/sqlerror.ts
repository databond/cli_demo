interface SqlError {
    errorCode: string,
    type: string,
    description: string,
}

