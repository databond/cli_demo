import IConnection from './connection';
export default interface ISession {
    Id: number,
    name: string,
    connections: IConnection[]
}